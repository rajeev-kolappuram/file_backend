const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');

const authRoutes = require('./routes/auth');
const fileRoutes = require('./routes/file');

dotenv.config();
const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));
app.use('/uploads', express.static('uploads'));
app.use('*/', cors());
app.listen(port);

console.log('\n');
console.log("\x1b[36m%s\x1b[0m", "App Running Port " + port, '\n');

let connection_url = "mongodb://localhost:27017/file";
let options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
mongoose.connect(connection_url, options);

app.use('/', authRoutes);
app.use('/file', fileRoutes);

module.exports = { app };
