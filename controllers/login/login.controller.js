const User = require('../../models/users');
const { decrytpion } = require('../../helpers/encrypt');

const jwt = require('jsonwebtoken');

async function loginAction(req, res) {
    const data = req.body;
    const user = await User.findOne({"email": data.email});

    if(user) {
        const isCorrect = await decrytpion(data.password, user.password);

        if(isCorrect) {
            const payload = { id: user.id, email: user.email, type: user.type };
            const token = jwt.sign(payload, 'secret', { expiresIn: 31556926 });

            return res.json({ status: 1, token: token, message: 'Login Success', data: user, errors: {} });
        } else {
            return res.json({ status: 2, message: 'Bad request.', data: {}, errors: { password: 'Password mismatch.' } });
        }
    } else {
        return res.json({ status: 2, message: 'Bad request.', data: {}, errors: { email: 'Email does not exist in database.' } });
    }
}

module.exports = {
    loginAction
}