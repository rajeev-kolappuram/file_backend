const { body } = require('express-validator');

const loginValidations = () => {
    return[
        body('email')
            .isEmail().withMessage("email is invalid.")
            .isString().withMessage("email must be string.")
            .not().isEmpty().withMessage("email is required."),
        body('password')
            .isString().withMessage("Password must be string.")
            .not().isEmpty().withMessage("Password is required.")
    ]
}

module.exports = {
    loginValidations
}