const File = require('../../models/files');

async function fileUpload(req, res) {
    const files = req.files;

    const file = new File({
        path: files.file[0].path,
        create_by: req.user.id,
        original_name: files.file[0].originalname
    });

    await file.save();

    return res.status(200).json({ status: 1, message: 'File uploaded sucessfully.', data: {}, errors: {} });
}

async function fileList(req, res) {
    var userId = '';
    const page = req.query.page ? req.query.page : 1;
    const search = req.query.search ? req.query.search : '';
    const perPage = 7;
    const offset = (page - 1) * [perPage];

    if(req.user.type === 'user') {
        userId = req.user.id;
    }

    const files = await File.aggregate([
        { "$match": { 'create_by': { $regex: '.*'+ userId +'.*' } } },
        { "$match": { 'original_name': { $regex: '.*'+ search +'.*' } } },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: page, per_page: perPage, } }],
                files: [{ $skip: offset }, { $limit: perPage }, {
                    $project: {
                        _id: 1,
                        path: { $concat: [process.env.APP_URL, "$path"] },
                        original_name: 1
                    }
                }],
            },
        },
        { $unwind: '$metadata' },
        {
            $project: {
                files: 1,
                page: "$metadata.page",
                per_page: "$metadata.per_page",
                total: "$metadata.total",
            }
        }
    ]);

    if(files.length !== 0) {
        return res.status(200).json({ status: 1, message: 'File list.', data: files, errors: {} });
    } else {
        return res.json({ status: 0, message: 'Bad request.', data: {}, errors: 'No files exist.' });    }
}

module.exports = {
    fileUpload,
    fileList
}