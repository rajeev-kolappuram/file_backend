const User = require('../../models/users');
const { encryption } = require('../../helpers/encrypt');

const ObjectId = require('mongoose').Types.ObjectId;

async function registerAction(req, res) {
    const data = req.body;
    const userExist = await User.count({"email": data.email});

    if(userExist) {
        return res.status(404).json({ status: 0, message: 'Bad request.', data: {}, errors: { email: 'User exist with same email' } })
    } else {
        const encrypt = await encryption(data.password);
        const newUser = new User({
            email: data.email,
            name: data.name,
            mobile: data.mobile,
            password: encrypt.hash,
            salt: encrypt.salt
        });

        newUser.save();

        return res.status(200).json({ status: 1, message: 'Register Success.', data: {}, errors: {} });
    }
}

async function getUserDetails(req, res) {
    const userId = req.user.id;
    const user = await User.aggregate([
        { $match: { "_id": ObjectId(userId) } },
        {
            $project: {
                email: 1,
                name: 1,
                mobile: 1,
                created_at: { $dateToString: { format: "%d-%m-%Y %H:%M:%S", date: "$created_at" } }
            }
        }
    ])

    return res.status(200).json({ status: 1, message: 'Details fetched.', data: user, errors: {} });
}

module.exports = {
    registerAction,
    getUserDetails
}