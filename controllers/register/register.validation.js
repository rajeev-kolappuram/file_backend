const { body } = require('express-validator');

const registerValdations = () => {
    return [
        body('email')
            .isEmail().withMessage("Input must be email")
            .isString().withMessage('Email must be number.')
            .not().isEmpty().withMessage("Email is required."),
        body('mobile')
            .isNumeric().withMessage('Mobile number must be string.')
            .not().isEmpty().withMessage("Mobile number is required."),
        body('name')
            .isString().withMessage('Name must be string.')
            .not().isEmpty().withMessage('Name is required.'),
        body('password')
            .isLength({ min: 6 }).withMessage('Password must be 6 characters long')
            .isString().withMessage('Password must be string')
            .not().isEmpty().withMessage('Password is required'),
        body('confirm_password')
            .custom((value, { req, loc, path }) => { return (req.body.password === req.body.confirm_password) ? true : false }).withMessage('Password and confirm password must match')
            .isLength({ min: 6 }).withMessage('Confirm password must be 6 characters long')
            .isString().withMessage('Confirm password must be string')
            .not().isEmpty().withMessage('Confirm password is required')
    ]
}

module.exports = {
    registerValdations
}