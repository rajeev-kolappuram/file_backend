const mongoose = require('mongoose');

const FilesSchema = new mongoose.Schema({
    path: {
        type: String,
        required: true
    },
    original_name: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    create_by: {
        type: Object,
        required: true
    }
});

module.exports = Files = mongoose.model('files', FilesSchema);
