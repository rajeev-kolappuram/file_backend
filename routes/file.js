const express = require('express');
const multer = require('multer');
const path = require('path');

const { fileUpload, fileList } = require('../controllers/file/file.controller');
const checkAuth = require('../helpers/checkAuth');

const router = express.Router();
const storage = multer.diskStorage({
    destination: (req, res, callback) => {
        callback(null, 'uploads');
    },
    filename: (req, file, callback) => {
        callback(null, file.originalname.split('.')[0] + "-" + Date.now() + path.extname(file.originalname));
    }
});
const upload = multer({ storage: storage });

router.post('/upload', checkAuth, upload.fields([{ name: 'file', maxCount: 1 }]), function(req, res) {
    const files = req.files;

    if(files === undefined || files === null || files.file === undefined || files.file === null) {
        return res.status(422).json({ status: 2, message: 'Bad request.', data: {}, errors: { file: 'File is not uploaded.' } });
    } else {
        if(path.extname(files.file[0].filename) !== '.pdf') {
            return res.status(422).json({ status: 2, message: 'Bad request.', data: {}, errors: { file: 'PDF files are only allowed.' } });
        } else {
            fileUpload(req, res);
        }
    }
});

router.get('/list', checkAuth, (req, res) => {
    fileList(req, res);
});

module.exports = router;
