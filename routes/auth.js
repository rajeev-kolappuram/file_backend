const express = require('express');

const { validate } = require('../helpers/validate');
const { loginValidations } = require('../controllers/login/login.validation');
const { loginAction } = require('../controllers/login/login.controller');
const { registerAction, getUserDetails } = require('../controllers/register/register.controller');
const { registerValdations } = require('../controllers/register/register.validation');
const checkAuth = require('../helpers/checkAuth');

const router = express.Router();

router.post('/login', loginValidations(), validate, function(req, res) {
    loginAction(req, res);
});

router.post('/register', registerValdations(), validate, async function(req, res) {
    registerAction(req, res);
});

router.get('/user-details', checkAuth, function(req, res) {
    getUserDetails(req, res);
});

module.exports = router;
