const jwt = require("jsonwebtoken");

module.exports = function (req, res, next) {
    if (req.headers && req.headers.authorization) {
        var authorization = req.headers.authorization.split(' ')[1], decoded;
        try {
            const verified = jwt.verify(authorization, 'secret');
            req.user = verified;
            next();
        } catch (e) {
            return res.status(422).json({ status: 0, message: 'Unauthenticated, Please login and try again.', data: {}, errors: {} });
        }
    } else {
        return res.status(422).json({ status: 0, message: 'Token not found. Please login and try again.', data: {}, errors: {} });
    }
};