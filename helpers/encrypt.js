var bcrypt = require('bcrypt');

async function encryption(password) {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(password, salt);

    return {
        "hash": hash,
        "salt": salt
    };
}

async function decrytpion(plainPass, hashword) {
    return await bcrypt.compare(plainPass, hashword);
}

module.exports = {
    encryption,
    decrytpion
}
