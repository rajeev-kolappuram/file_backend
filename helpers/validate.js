const { validationResult } = require('express-validator');

const validate = (req, res, next) => {
    const errors = validationResult(req);

    if(errors.isEmpty()) {
        return next();
    }

    const result = {};
    errors.array().forEach(element => {
        result[element.param] = element.msg;
    })

    return res.status(422).json({ status: 2, message: 'Bad request.Validation errors found.', data: {}, errors: result });
}

module.exports = {
    validate
}